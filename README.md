# Antoni Segura Puimedon SVGs

These are the SVGs I collected or made with Inkscape to aid me in making slides
for my presentations and documentations.

I recommend all of them be opened and rendered with Inkscape (and please
support that project! It is awesome!).
